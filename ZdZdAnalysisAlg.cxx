
#ifdef XAOD_ANALYSIS //we use quickana, which is only available in the analysis releases

// ZdZdAnalysis includes
#include "ZdZdAnalysisAlg.h"

#include "xAODTracking/VertexContainer.h"

#include "xAODMuon/Muon.h"

#include "xAODJet/Jet.h"

#include "ZdZdHelper.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "xAODEgamma/ElectronxAODHelpers.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "xAODCore/ShallowCopy.h"

#include "xAODTruth/TruthParticleContainer.h"
#include "HZZUtils/OverlapRemoval.h"

std::vector<std::pair<std::string,float>> ZdZdHelper::leptonSFSysts;

ZdZdAnalysisAlg::ZdZdAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) : CamAlgorithm( name, pSvcLocator ),
   m_gotBookkeeper(false), 
   quickAna(""),
   chanInfo("ChannelInfoTool/channelInfo"),
   m_muCalibTool(""),m_elCalibTool(""),
   m_muSelectionTool(""),
   m_elLikelihoodTool(""),m_elLikelihoodToolMedium(""),m_elLikelihoodToolVLoose(""),
   m_prwTool(""),
   m_grlTool(""),
   m_tdtTool(""),
   m_jvtTool("CP::JetJvtEfficiency/JVT"),m_fjvtTool("JetForwardJvtTool/ForwardJVT"),
   m_jetCleaningTool(""),m_jetCalibTool(""),m_orTool(""),m_tmt(""),
   m_isoCorrectionTool(""),m_ffTool("ParticleScaleFactorTool/FakeFactorTool")

{
   
   declareProperty( "QuickAna", quickAna ); 
   declareProperty("MuonCalibTool",m_muCalibTool);
   declareProperty("ElectronCalibTool",m_elCalibTool);
   declareProperty("MuonSelectionTool",m_muSelectionTool );
   declareProperty("ElectronLikelihoodTool",m_elLikelihoodTool); declareProperty("ElectronLikelihoodToolMedium",m_elLikelihoodToolMedium); declareProperty("ElectronLikelihoodToolVLoose",m_elLikelihoodToolVLoose);  declareProperty("ElectronLikelihoodToolTight",m_elLikelihoodToolTight);
   declareProperty("PileupReweightingTool",m_prwTool);
   declareProperty("GRLTool",m_grlTool);
   declareProperty("TrigDecisionTool",m_tdtTool);
   declareProperty("JetCleaningTool",m_jetCleaningTool);
   declareProperty("JetCalibTool",m_jetCalibTool);
   declareProperty("OverlapRemovalTool", m_orTool);
   declareProperty("TriggerMatchingTool", m_tmt);

   //these are ToolHandleArrays
   declareProperty("IsolationSelectionTools",m_isoSelectionTools);
   declareProperty("IsolationHelpers", m_isolHelpers);

   declareProperty("IsolationCorrectionTool", m_isoCorrectionTool); //corrects electron isolation vars

   declareProperty("ElectronSFTools",m_elSFTools);declareProperty("ElectronSFTools2",m_elSFTools2);
   declareProperty("MuonSFTools",m_muSFTools);

   
   declareProperty("LeptonSFSystematics",m_leptonSFSysts);declareProperty("LeptonSFSystematics2",m_leptonSFSysts2);

   declareProperty("NoSkimChannels",m_noSkimChannels,"List of mc channel numbers that we do not skim on");
   
   declareProperty("Systematics",m_systematics=std::vector<std::string>({"Nominal"}),"List of systematics to run on");

   declareProperty("Triggers",m_trigs,"if specified, we will filter cutflow on list of given triggers");
   
   declareProperty("SkipJets",skipJets=false,"if true, doesn't look at jets");
   declareProperty("DoTauVeto",m_doTauVeto=false,"if true, will not include tau events in cutflow .. only here for SMZZ cutflow");
   declareProperty("PrintCutflow",m_printCutflow=false,"if true, will print a cutflow at end of job");

}


ZdZdAnalysisAlg::~ZdZdAnalysisAlg() {}

StatusCode ZdZdAnalysisAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

   CHECK(m_tdtTool.retrieve());

   debugfile.open("debug.txt");

  if(!m_printCutflow) {
    //disable printing in the chanInfo tool
    CHECK(dynamic_cast<AlgTool*>(&*chanInfo)->setProperty("PrintCutflow",false));
  }

  for(auto& t : m_trigs) {
    m_triggers.insert(t);
    std::cout << t << std::endl;
  }
  
  
  m_myTriggers[11].push_back(Trigger(0,"HLT_e24_lhmedium_L1EM20VH"));  //0-284484 (2015)
  m_myTriggers[11].push_back(Trigger(1,"HLT_e60_lhmedium")); //0-284484
  m_myTriggers[13].push_back(Trigger(2,"HLT_mu20_iloose_L1MU15","HLT_mu20_iloose")); //0-300287 (periodA end)
  m_myTriggers[13].push_back(Trigger(3,"HLT_mu40")); //0-300287
  m_myTriggers[22].push_back(Trigger(4,"HLT_2e12_lhloose_L12EM10VH")); //0-284484
  m_myTriggers[26].push_back(Trigger(5,"HLT_mu18_mu8noL1")); //0-284484
  m_myTriggers[26].push_back(Trigger(6,"HLT_2mu10")); //0-300287
  m_myTriggers[24].push_back(Trigger(7,"HLT_e17_lhloose_mu14")); //0-284484
  m_myTriggers[33].push_back(Trigger(8,"HLT_e17_lhloose_2e9_lhloose"));  //0-284484
  m_myTriggers[39].push_back(Trigger(9,"HLT_3mu6"));  //0->
  
  m_myTriggers[11].push_back(Trigger(10,"HLT_e26_lhtight_nod0_ivarloose")); //296639-> (2016)
  m_myTriggers[11].push_back(Trigger(11,"HLT_e60_lhmedium_nod0")); //296639-> (2016)
  m_myTriggers[13].push_back(Trigger(12,"HLT_mu50")); //0->
  m_myTriggers[22].push_back(Trigger(13,"HLT_2e17_lhvloose_nod0")); //296639-> (2016)
  m_myTriggers[26].push_back(Trigger(14,"HLT_mu22_mu8noL1")); //0->
  m_myTriggers[24].push_back(Trigger(15,"HLT_e17_lhloose_nod0_mu14")); //296639-> (2016)
  m_myTriggers[33].push_back(Trigger(16,"HLT_e17_lhloose_nod0_2e9_lhloose_nod0")); //296639-> (although technically was prescaled briefly in period G->)

  //Extra triggers, as proposed by Cong to harmonise with HSG2: https://indico.cern.ch/event/572810
  m_myTriggers[26].push_back(Trigger(17,"HLT_mu20_mu8noL1")); // 0 -> 302393
  m_myTriggers[26].push_back(Trigger(18,"HLT_2mu14")); // 0 ->
  m_myTriggers[33].push_back(Trigger(19,"HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH")); // 305291-> (period G->)
  m_myTriggers[13].push_back(Trigger(20,"HLT_mu24_ivarmedium")); // 296639 -> 302393
  m_myTriggers[13].push_back(Trigger(21,"HLT_mu26_ivarmedium")); // 296639 ->
  
  m_myTriggers[26].push_back(Trigger(22,"HLT_mu20_nomucomb_mu6noL1_nscan03")); //296639->302393 (up end of to period C)

  //m_myTriggers[11].push_back(Trigger(22,"HLT_e120_lhloose")); // 0->284484 (to use SF files)
  //m_myTriggers[11].push_back(Trigger(23,"HLT_e140_lhloose_nod0")); // 296639 -> (2016) (to use SF files)

  return StatusCode::SUCCESS;
}

#include "PATInterfaces/MakeSystematicsVector.h"

StatusCode ZdZdAnalysisAlg::firstExecute() {
  ATH_MSG_INFO("In firstExecute ... ");

  CamEvent evt;

  //add this list of triggers to the output nominal tree
  ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
  TTree* tree = 0;
  if(histSvc->getTree( "/NTUP4L/Nominal/llllTree", tree ).isSuccess()) {
    uint i=0;
    while(i<32) {
      std::string trigName;
      for(auto& t : m_myTriggers) {
        for(auto& tt : t.second) {
          if(tt.idx==i) { trigName = tt.getTrigger(!evt.is("isSimulation")); break; }
        }
        if(!trigName.empty()) break;
      }
      if(trigName.empty()) break; //not found so end loop
      tree->GetUserInfo()->Add( new TNamed(trigName.c_str(),TString::Format("%d",i).Data()) );
      i++;
    }
  }


  //also do the systematics setup, only for MC
  if(evt.is("isSimulation")) {
    m_sfSysts.resize(1); //adds a 'nominal' systematic variation
    for(auto& s : m_leptonSFSysts) {
      CP::SystematicSet tmpSet;
      if(s.find("toy") != std::string::npos) {
        TString ss(s); 
        tmpSet.insert(CP::SystematicVariation::makeToyVariation(ss(0,ss.Index("toy")-2),TString(ss(ss.Index("toy_")+4,ss.Index("_",ss.Index("toy_")+4)-(ss.Index("toy_")+4))).Atoi(), 1.));
        m_sfSysts.push_back(tmpSet);
      } else {
        tmpSet.insert(CP::SystematicVariation(s,1));m_sfSysts.push_back(tmpSet);
        tmpSet.clear();
        tmpSet.insert(CP::SystematicVariation(s,-1));m_sfSysts.push_back(tmpSet);
      }
    }
    
    m_sfSysts2.resize(0);
    for(auto& s : m_leptonSFSysts2) {
      CP::SystematicSet tmpSet;
      tmpSet.insert(CP::SystematicVariation(s,1));m_sfSysts2.push_back(tmpSet);
      tmpSet.clear();
      tmpSet.insert(CP::SystematicVariation(s,-1));m_sfSysts2.push_back(tmpSet);
    }
    
    //initialize the map in the ZdZdHelper from property name to float
    //uses a vector to keep in same order as systs vector above
    ZdZdHelper::leptonSFSysts.clear();
    for(auto& s : m_sfSysts) {
      ZdZdHelper::leptonSFSysts.push_back(std::make_pair("scaleFactor" + s.name(),1.));
      std::cout << "scaleFactor"  << s.name() << std::endl;
    }
    for(auto& s : m_sfSysts2) {
      ZdZdHelper::leptonSFSysts.push_back(std::make_pair("scaleFactor" + s.name(),1.));
      std::cout << "scaleFactor"  << s.name() << std::endl;
    }
    
  } else {
    ZdZdHelper::leptonSFSysts.clear();m_sfSysts.clear();m_sfSysts2.clear(); //no scale factors for data
    m_systematics.clear();m_systematics.push_back("Nominal"); //only Nominal in data
  }


  for(auto& sy : m_systematics) {
    if(sy=="Nominal") m_systs.push_front(std::make_pair("Nominal",CP::SystematicSet()));
    else {
      CP::SystematicSet tmpSet;
      CP::SystematicVariation s(sy,1); tmpSet.insert(s); TString tt(s.name()); tt.ReplaceAll("__","");m_systs.push_back(std::make_pair(tt.Data(),tmpSet));
      tmpSet.clear();
      CP::SystematicVariation s2(sy,-1); tmpSet.insert(s2); TString tt2(s2.name()); tt2.ReplaceAll("__","");m_systs.push_back(std::make_pair(tt2.Data(),tmpSet));
    }
  }
  
  if(evt.is("isSimulation")) { 
    for(auto& tool : m_elSFTools) {
      CP::SystematicSet recSysts = tool->recommendedSystematics();   
      for(auto& syst : recSysts) {
        ATH_MSG_DEBUG("RECOMMENDED SYST = " << syst);
      }
      CP::MakeSystematicsVector sysVecToys;
      sysVecToys.addGroup("toys");
      sysVecToys.setToys(  40   );
      sysVecToys.calc(recSysts);
      std::vector<CP::SystematicSet> sysSetToys=sysVecToys.result("toys");
      for(auto& sy : sysSetToys) {
      ATH_MSG_DEBUG("TOY = " << sy.name());
      }
    }
  }


  return StatusCode::SUCCESS;
}


//include other methods
#include "ZdZdAnalysisAlg_preSelection.cxx"
#include "ZdZdAnalysisAlg_elSelection.cxx"
#include "ZdZdAnalysisAlg_muSelection.cxx"
#include "ZdZdAnalysisAlg_jetSelection.cxx"

#include "AthenaKernel/IEventSeek.h"

StatusCode ZdZdAnalysisAlg::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "..." << m_isNoSkimChannel);

   m_eventOK=true; //when goes false, we stop filling cutflow, even though we keep 'going'

   CamEvent evt;
   
   evt.set("evtWeight") = (evt.is("isSimulation")) ? double(evt.ae<xAOD::EventInfo>()->mcEventWeight()) : 1.;
   //evt.set("EventNumber") = int(evt.ae<xAOD::EventInfo>()->eventNumber());
   if(!m_gotBookkeeper) chanInfo->Fill("Events_All",evt.get("evtWeight"));
   chanInfo->Fill("Events_Processed",evt.get("evtWeight"));
   if(msgLvl(MSG::VERBOSE)) evt.print();

   
   
   //do the preselection (vertex + trigger requirement)
   chronoStatService()->chronoStart( "ZdZdAnalysis::preExecute" );
   StatusCode sc = preExecute(evt);
   chronoStatService()->chronoStop( "ZdZdAnalysis::preExecute" );
   if(sc.isFailure()) {
    if(sc.isRecoverable()) { 
      if(m_isNoSkimChannel) evt.applyService("CamWriter/NominalWriter");
      ATH_MSG_VERBOSE("Event did not pass preselections"); return StatusCode::SUCCESS; }  //failed preselections
    ATH_MSG_ERROR("Failure in preExecute"); return StatusCode::FAILURE; 
   }

   bool noSkim = m_isNoSkimChannel;m_doingNominal=true;

   //copy over truth_zdzd_avgM value, so it gets into variation trees
   if(evt.contains<float>("truth_zdzd_avgM",false,true,false,false,false)) evt.set("truth_zdzd_avgM") = evt.get<float>("truth_zdzd_avgM");
   //same for hasTau
   if(evt.contains<bool>("truth_hasTau",false,true,false,false,false)) evt.set("truth_hasTau") = evt.get<bool>("truth_hasTau");
   if(evt.contains<float>("ewWeight",false,true,false,false,false)) evt.set("ewWeight") = evt.get<float>("ewWeight");

   for(auto& syst : m_systs) {
     if(syst.first!="Nominal") {noSkim=false;m_eventOK=false;m_doingNominal=false;} //only disable skimming in nominal case ... also only cutflow in nominal case
     if(!m_doingNominal) chronoStatService()->chronoStart( "ZdZdAnalysis::subExecute_NotNominal" );
     chronoStatService()->chronoStart( "ZdZdAnalysis::subExecute_" + syst.first );
     StatusCode sc = subExecute(evt, noSkim, syst.first, syst.second);
     if(sc.isRecoverable() && m_isNoSkimChannel && m_doingNominal) { evt.applyService("CamWriter/" + syst.first + "Writer"); }
     chronoStatService()->chronoStop( "ZdZdAnalysis::subExecute_" + syst.first );
     if(!m_doingNominal) chronoStatService()->chronoStop( "ZdZdAnalysis::subExecute_NotNominal" );
     if(sc.isFailure() && !sc.isRecoverable()) { ATH_MSG_ERROR("Failure in subExecute " << syst.first); return StatusCode::FAILURE; }
     
   }

    return StatusCode::SUCCESS;
}


StatusCode ZdZdAnalysisAlg::subExecute(CamEvent& evt, bool noSkim, const std::string& syst, const CP::SystematicSet& theSyst) {


  const xAOD::ElectronContainer* theElectrons = nullptr; const xAOD::MuonContainer* theMuons = nullptr; const xAOD::JetContainer* theJets = nullptr;
  

  //std::unique_ptr<xAOD::ElectronContainer> theElectrons = nullptr; std::unique_ptr<xAOD::MuonContainer> theMuons = nullptr; 
  //std::unique_ptr<xAOD::ShallowAuxContainer> theElectronsAux = nullptr; std::unique_ptr<xAOD::ShallowAuxContainer> theMuonsAux = nullptr;
  
  


  CamDeque* els_select = 0;
  CamDeque* mus_select = 0;
  CamDeque* jets_select = 0;
  

  if( m_doingNominal || boost::starts_with(theSyst.begin()->basename(),"EG_") ) {
    //need new electrons
  
    const xAOD::ElectronContainer* xaod_els = 0;CHECK( evtStore()->retrieve(xaod_els,"Electrons") );
    auto calibratedElectrons = xAOD::shallowCopyContainer( *xaod_els );
    //now actually calibrate them
    m_elCalibTool->applySystematicVariation( theSyst ).ignore();
    for(auto el : *calibratedElectrons.first) {
      chronoStatService()->chronoStart( "ZdZdAnalysis::calibrate_electrons" );
      m_elCalibTool->applyCorrection(*el).setChecked();
      m_isoCorrectionTool->applyCorrection(*el).setChecked(); //isolation variable corrections
      chronoStatService()->chronoStop( "ZdZdAnalysis::calibrate_electrons" );
    }
    //record to storegate so that it's cleaned up for us
    CHECK( evtStore()->record( calibratedElectrons.first , "Electrons__" + syst) ); CHECK( evtStore()->record( calibratedElectrons.second , "Electrons__" + syst + "Aux.") );
    theElectrons = calibratedElectrons.first; 
    
    if(m_doingNominal) m_theElectrons =  calibratedElectrons.first ;
  
    CamVector* els = evt.createVector("electrons__" + syst,theElectrons);
    
    //if(msgLvl(MSG::VERBOSE)) els->all_print();
    
    chronoStatService()->chronoStart( "ZdZdAnalysis::select_electrons" );
    els_select = els->subsetDeque( CamObject::Function(&ZdZdAnalysisAlg::electronSelection,this) );
    chronoStatService()->chronoStop( "ZdZdAnalysis::select_electrons" );
  
  } else {
    els_select = evt.getDeque("electrons__Nominal_subset1");
    theElectrons = m_theElectrons;
  }
    
  if( m_doingNominal || theSyst.begin()->basename()=="MUON_ID" || theSyst.begin()->basename()=="MUON_MS" || theSyst.begin()->basename()=="MUON_SCALE"/*boost::starts_with(theSyst.begin()->basename(),"MUONS_")*/ ) {
    //new new muons
    
    const xAOD::MuonContainer* xaod_mus = 0;CHECK( evtStore()->retrieve(xaod_mus,"Muons") );
    chronoStatService()->chronoStart( "ZdZdAnalysis::calibrate_muons" );
    auto calibratedMuons = xAOD::shallowCopyContainer( *xaod_mus );
    m_muCalibTool->applySystematicVariation( theSyst ).ignore();
    for(auto mu : *calibratedMuons.first) {
       m_muCalibTool->applyCorrection(*mu).setChecked();
    }
    chronoStatService()->chronoStop( "ZdZdAnalysis::calibrate_muons" );
    //theMuons.reset(calibratedMuons.first); theMuonsAux.reset(calibratedMuons.second);
    CHECK( evtStore()->record( calibratedMuons.first , "Muons__" + syst) ); CHECK( evtStore()->record( calibratedMuons.second , "Muons__" + syst + "Aux.") );
    theMuons = calibratedMuons.first; 
    if(m_doingNominal) m_theMuons =  calibratedMuons.first ;
    
    CamVector* mus = evt.createVector("muons__" + syst,theMuons);
    
    //if(msgLvl(MSG::VERBOSE)) mus->all_print();
  
    chronoStatService()->chronoStart( "ZdZdAnalysis::select_muons" );
    mus_select = mus->subsetDeque( CamObject::Function(&ZdZdAnalysisAlg::muonSelection,this) );
   chronoStatService()->chronoStop( "ZdZdAnalysis::select_muons" );
    
  } else {
    mus_select = evt.getDeque("muons__Nominal_subset1");
    theMuons = m_theMuons;
  }
  
  if(skipJets) {
    jets_select = 0;
    theJets = 0;
  } else {
      //old method
//    if( m_doingNominal || boost::starts_with(theSyst.begin()->basename(),"JET_") ) {
//      //new new jets
//      const xAOD::JetContainer* xaod_jets = 0;  CHECK( evtStore()->retrieve( xaod_jets , "AntiKt4EMTopoJets" ) );
//      chronoStatService()->chronoStart( "ZdZdAnalysis::calibrate_jets" );
//      auto calibratedJets = xAOD::shallowCopyContainer( *xaod_jets );
//      if(!m_jetCalibTool.empty()) {
//        //m_jetCalibTool->applySystematicVariation( theSyst ).ignore();
//        for(auto jet : *calibratedJets.first) {
//          m_jetCalibTool->applyCorrection(*jet).setChecked();
//        }
//      }
//      m_fjvtTool->modify(*calibratedJets.first); //decorates with passFJVT char
//      chronoStatService()->chronoStop( "ZdZdAnalysis::calibrate_jets" );
//    
//      CHECK( evtStore()->record( calibratedJets.first , "AntiKt4EMTopoJets__" + syst) ); CHECK( evtStore()->record( calibratedJets.second , "AntiKt4EMTopoJets__" + syst + "Aux.") );
//      theJets = calibratedJets.first; 
//      if(m_doingNominal) m_theJets =  calibratedJets.first ;
//      CamVector* jets = evt.createVector("jets__" + syst,theJets);
//      chronoStatService()->chronoStart( "ZdZdAnalysis::select_jets" );
//      jets_select = jets->subsetDeque( CamObject::Function(&ZdZdAnalysisAlg::jetSelection,this) );
//      chronoStatService()->chronoStop( "ZdZdAnalysis::select_jets" );
//    } else {
//      jets_select = evt.getDeque("jets__Nominal_subset1");
//      theJets = m_theJets;
//    }
    //New method: using CamJetProvider
    chronoStatService()->chronoStart( "ZdZdAnalysis::calibrate_jets" );
    jets_select = evt.getDeque("CamJetProvider/zdzdJets",theSyst);
    //Justin comments
    const xAOD::JetContainer* xaod_jets = 0;
    CHECK( evtStore()->retrieve( xaod_jets , "AntiKt4EMTopoJets" ) );
    CamVector* jets = evt.getVector("AntiKt4EMTopoJets");
//    for (const auto rawjet: *jets) {
//      ATH_MSG_INFO("jet eta = " << rawjet->eta());
//    }
    //
    theJets = static_cast<const xAOD::JetContainer*>(jets_select->container());
    chronoStatService()->chronoStop( "ZdZdAnalysis::calibrate_jets" );
  }


   //ATH_MSG_INFO("el selected: " << els->size("ana_select") << " . mu selected: " << mus->size("ana_select") );





chronoStatService()->chronoStart( "ZdZdAnalysis::overlapRemoval" );

   //electron-electron track and calo overlap removal
   //doEEOverlapRemoval(els_select);

   //run OR tool ..
   CHECK( m_orTool->removeOverlaps(theElectrons,theMuons, theJets ) );

   int n_el_ord(0); int n_mu_ord(0);

   if(msgLvl(MSG::VERBOSE)) {
    ATH_MSG_VERBOSE("Overlaps results: ");
    els_select->all_print("tlv,selected,overlaps");
    mus_select->all_print("tlv,selected,overlaps");
   }

   //adjust the overlaps decoration to encode what type of object was ORd by
   for(auto el = els_select->begin(); el != els_select->end(); ++el) {
      //also for ORd lepton flag how it was ORd. Bits will be: elRemovedByEl,byMu,byJet, muRemovedByEl,byMu,byJet
      if((*el)->auxdecor<char>("overlaps")) {
         int ordBy = 0;
         if(!ordBy) {
            //retrieve object link and see if it was an electron or muon
            auto orObj = (*el)->auxdecor<ElementLink<xAOD::IParticleContainer>>("overlapObject");
            switch((*orObj)->type()) {
               case xAOD::Type::Electron: ordBy = 4; n_el_ord++; std::cout << "e OR by e" << std::endl;  break;
               case xAOD::Type::Muon: ordBy = 2;  n_el_ord++; break;
               case xAOD::Type::Jet: 
                  if((*orObj)->auxdata<char>("involume")==false) ordBy = 16; //or'd by an out-of-volume jet
                  else ordBy = 1;
                  break;
               default: ATH_MSG_FATAL("Unknown OR type: " << (*orObj)->type());
                        return StatusCode::FAILURE;
            }
         }
         //bitshift by 3 for electrons:
         ordBy = ordBy << 3;
         (*el)->auxdecor<char>("overlaps") = ordBy;
      }

   }

   for(auto mu = mus_select->begin(); mu != mus_select->end(); ++mu) {
      //also for ORd lepton flag how it was ORd
      if((*mu)->auxdecor<char>("overlaps")) {
         int ordBy = 0;
         if(!ordBy) {
            //retrieve object link and see if it was an electron or muon
            auto orObj = (*mu)->auxdecor<ElementLink<xAOD::IParticleContainer>>("overlapObject");
            switch((*orObj)->type()) {
               case xAOD::Type::Electron: ordBy = 4;  n_mu_ord++; break;
               case xAOD::Type::Muon: ordBy = 2;  n_mu_ord++; break;
               case xAOD::Type::Jet: 
                  if((*orObj)->auxdata<char>("involume")==false) ordBy = 64; //or'd by an out-of-volume jet
                  else ordBy = 1;
                  break;
               default: ATH_MSG_FATAL("Unknown OR type: " << (*orObj)->type());
                        return StatusCode::FAILURE;
            }
         }
         (*mu)->auxdecor<char>("overlaps") = ordBy;
      }
   }
   
   
   
   evt.set("hasUncleanJets") = false;evt.set("hasORdUncleanJets") = false;
   //go through jets, if any are selected and unclean, then set flag true
   if(!skipJets) {
    for(const auto& jet : *theJets) {
      if(jet->auxdata<char>("unclean")) {
        if(jet->auxdata<char>("selected")) { evt.set("hasUncleanJets")=true; }
        else { evt.set("hasORdUncleanJets")=true; }
      }
    }
    if(m_printCutflow && m_eventOK && !evt.get<bool>("hasUncleanJets")) chanInfo->Fill("Events_passJetCleaning",evt.get("evtWeight"));
   }

chronoStatService()->chronoStop( "ZdZdAnalysis::overlapRemoval" );

   if(m_printCutflow && m_eventOK) chanInfo->Fill("Electrons_postOR2", els_select->size()-n_el_ord);
   //int mu_removed = mus_select->size("overlaps");
   if(m_printCutflow && m_eventOK) chanInfo->Fill("Muons_postOR",mus_select->size()-n_mu_ord);



   //need at least 4l
   if( (els_select->size()+mus_select->size())<4 ) return StatusCode::RECOVERABLE;
   //if have fewer than 2 muons, than need at least 2 good electrons, we never relax more than 2 of the electrons
   if(mus_select->size()<2 && els_select->size("selected")<2) return StatusCode::RECOVERABLE;
   
   
   

   evt.set("pass4l") = true;

   //tag electrons and muons with pdgId
   els_select->all_call( [](CamObject& c) { c.set("pdgId") = (c.get<float>("charge")<0) ? 11 : -11; } );
   mus_select->all_call( CamObject::Function([](CamObject& c) { c.set("pdgId") = (c.get<float>("charge")<0) ? 13 : -13; }) );


   
   
   //apply scale factors ... only running systematics if doing the 'nominal' tree
chronoStatService()->chronoStart( "ZdZdAnalysis::applyElScaleFactors" );
   els_select->all_call(  CamObject::Function(&ZdZdAnalysisAlg::applyElScaleFactor,this) );
chronoStatService()->chronoStop( "ZdZdAnalysis::applyElScaleFactors" );
chronoStatService()->chronoStart( "ZdZdAnalysis::applyMuScaleFactors" );
   mus_select->all_call(  CamObject::Function(&ZdZdAnalysisAlg::applyMuScaleFactor,this) );
chronoStatService()->chronoStop( "ZdZdAnalysis::applyMuScaleFactors" );

   els_select->sort_Pt();
   mus_select->sort_Pt();
   

   
   
   

   //combine leptons into a single deque, ordered by pt, and label with index
   
   CamDeque l; l += (*els_select); l += (*mus_select);
   l.sort_Pt();for(uint i=0;i<l.size();i++) l[i]->set("l_index") = i;
   l.setParent(&evt);
   
   //add fake factors
   for(auto& a : l) {
     a->set("fakeFactor") = float(m_ffTool->evaluate(a->ae<xAOD::IParticle>()));
   }
   
   CamDeque* outJets = 0;
   
   
   if(!skipJets) {
        l.all_set("matched_j") = char(-1);
        //flag leptons that match to a jet and vice versa
        outJets = jets_select->subset([](CamObject& c){return !c.get<char>("overlaps");});
        outJets->sort_Pt();
        if(outJets->size()) {
          //create a deque of worse-than-loose electrons
          CamDeque badElectrons;
          for(auto el = l.begin(); el != l.end(); ++el) {
                (*el)->set("matched_j") = char(-1);
                if(abs((*el)->get<int>("pdgId")) != 11) continue;
                if((*el)->auxdecor<char>("overlaps")&54) continue; //el was ORd
                if((*el)->get<int>("quality")<=2) continue; //el was loose, so was used in OR already
                badElectrons.push_back(*el);
          }
          if(badElectrons.size()) {
            char ii=-1;
            for(auto& jet : *outJets) {
              ii++;
              //also check if a select jet is close enough to a worse-than-loose electron
              //since such leptons were not included in OR, we need to note the electron because if we use it in quadruplet
              //in inverted SR, we should remove that jet
              jet->set("matched_l") = -1;
              for(auto& el : badElectrons) {
                if( el->DeltaR(*jet) < 0.2 ) {
                  jet->set("matched_l") = char(el->get<uint>("l_index"));
                  el->set("matched_j") = ii;
                  //std::cout << "matched " << el->get<uint>("l_index") << " to " << int(ii) <<std::endl;
                  break; //only allow up to one matchup ... technically might not be correct to do, but want to take highest pt el as the match
                }
              }
            } //over jets
          }
        }
   }

   ATH_MSG_DEBUG("Making dileptons");
  chronoStatService()->chronoStart( "ZdZdAnalysis::makell" );
   //MUST do all ll in one go, otherwise end up with two sets of ll that share the same index numbers. Could have dont separately and incremented
   //the index numbers of second set of ll too, but decided to do in one go
   //this was why my eemm channel numbers were wrong (reported wrong massee (the same) because my llll_ll1 == llll_ll2 in some cases)
   CamVector ll = ZdZdHelper::makell(l,false,true/*allow same sign for now*/,false/*don't allow opposite flavour*/);
  chronoStatService()->chronoStop( "ZdZdAnalysis::makell" );


   ATH_MSG_DEBUG("Making dilepton pairs");
   //and then pair these ...
  chronoStatService()->chronoStart( "ZdZdAnalysis::makellll" );
   CamVector lllls = ZdZdHelper::makellll(ll,false/*not truth level*/,m_doingNominal,skipJets);
  chronoStatService()->chronoStop( "ZdZdAnalysis::makellll" );

  ATH_MSG_DEBUG("Trigger matching");
  chronoStatService()->chronoStart( "ZdZdAnalysis::decorateTriggerMatched" );
   decorateTriggerMatched(lllls,evt); //labels quadruplets as trigger matched or not
  chronoStatService()->chronoStop( "ZdZdAnalysis::decorateTriggerMatched" );

  chronoStatService()->chronoStart( "ZdZdAnalysis::decorateIsolations" );
   //add 'isXXXHelper' isolation flags (one for each isolation tool)
   decorateHelperIsolation(lllls);
   //add 'isXXXModified' isolation flags (one for each isolation tool)
   //decorateModifiedIsolation(lllls); //ALTERNATIVE TO USING THE HELPER TOOL
  chronoStatService()->chronoStop( "ZdZdAnalysis::decorateIsolations" );


  if(lllls.size()) {
    
    //NOTE: this only works on HIGG2D1 derivations
    const xAOD::VertexContainer* vtx4l = evtStore()->tryConstRetrieve<xAOD::VertexContainer>( "FourLeptonVertices" );
    if(vtx4l) {
      ATH_MSG_DEBUG("Vertex fitting");
      for(auto& llll : lllls) {
        float reduced_chi2 = -999;
        if(vtx4l) {
          std::set<const xAOD::TrackParticle*> tps;
          for(int i=0;i<2;i++) { for(int j=0;j<2;j++) {
            CamObject* obj = llll.child(i)->child(j);
            const xAOD::TrackParticle* tp = obj->get<const xAOD::TrackParticle*>("trackParticlePointer");
            if(tp) tps.insert(tp);
          } }
          
          //use tps set to find the vertex that was made with exactly these tps
          for(auto vtx : *vtx4l) {
            if(tps.size()!=vtx->nTrackParticles()) continue;
            bool goodVertex=true;
            for(unsigned int i=0;i<vtx->nTrackParticles();i++) {
              if(tps.find(vtx->trackParticle(i))==tps.end()) { goodVertex=false;break; }
            }
            if(!goodVertex) continue;
            //found the vertex, record the fit result
            reduced_chi2 = vtx->chiSquared()/vtx->numberDoF();
            break;
          }
        }
        llll.set("vtx_reduced_chi2") = reduced_chi2;
      }
   }
  }


  ATH_MSG_DEBUG("Writing");

 
chronoStatService()->chronoStart( "ZdZdAnalysis::writingNtuple" );
   if(lllls.size()>0) {
      const std::string w = "CamWriter/"+syst+"Writer";
      
      if(!skipJets) {
        //only keep the non-ORd jets
        outJets->applyService(w,"j_");
      }
      
      l.applyService(w,"l_");
      ll.applyService(w,"ll_");
      //evt.set("el_n") = int(els_select->size()); evt.set("mu_n") = int(mus_select->size());
      //add a dummy because of the ROOT bug
      //lllls.all_print("truePair");
      lllls.resize(lllls.size()+1); lllls.back().set("charge")=16; lllls.back().set("lexoz_rank")=uint(99);lllls.back().set("sumz_rank")=uint(99);
      lllls.applyService(w,"llll_");
      
      evt.applyService(w);
      //ATH_MSG_INFO("#llll = " << lllls.size());
      //lllls.all_print();
      
      if(msgLvl(MSG::VERBOSE)) {
        std::cout << "LEPTONS: " << std::endl;
        l.all_print();
        std::cout << "DILEPTONS: " << std::endl; 
        ll.all_print();
        std::cout << "QUADRUPLETS: " << std::endl;
        lllls.all_print();
      }
      
   } else {
      if(noSkim) evt.applyService("CamWriter/"+syst+"Writer"); //NOTE: means we dont write the jets if there isn't a quadruplet
   }
chronoStatService()->chronoStop( "ZdZdAnalysis::writingNtuple" );

  return StatusCode::SUCCESS;
}

#include "PATInterfaces/SystematicRegistry.h"

StatusCode ZdZdAnalysisAlg::finalize() {
   debugfile.close();
  ATH_MSG_INFO ("Finalizing " << name());

  if(m_printCutflow) {
   TString channelItems("pass_%s:SFOS_%s:Overlap_%s:Kinematic_%s:TriggerMatched_%s:DeltaR_%s:Isolated_%s:d0Sig_%s:ZVeto_%s:QVeto_%s:Recovered_%s");
   std::vector<std::string> c = {"4e","2e2m","4m","unknown"};
   TString t("Events_Processed:Events_passNPV:Events_passTrigger:Electrons:Electrons_IDCut:Electrons_etaCut:Electrons_etCut:Electrons_OQCut:Electrons_BASELINE:Muons:Muons_IDCut:Muons_EtaCut:Muons_ptCut:Muons_d0:Muons_BASELINE:Electrons_postOR2:Muons_postOR:Events_pass4l");
   for(uint i=0;i<c.size();i++) {
      TString s(channelItems);
      t+=":" + s.ReplaceAll("%s",c[i]);
   }
   chanInfo->Scan(t,"channels");
  }
  
  //double-check that all systematics were actually systematics (someone may change one under our feet)
  auto allSysts = CP::SystematicRegistry::getInstance().globalSystematics();
  for(auto s : m_systs) {
    CP::SystematicSet ss = s.second;
    for(auto sv : ss) {
      if(allSysts.find(sv)==allSysts.end()) ATH_MSG_WARNING("Could not find systematic " << sv.name());
    }
  }
  for(auto ss : m_sfSysts) {
    for(auto sv : ss) {
      if(allSysts.find(sv)==allSysts.end()) ATH_MSG_WARNING("Could not find systematic " << sv.name());
    }
  }
  for(auto ss : m_sfSysts2) {
    for(auto sv : ss) {
      if(allSysts.find(sv)==allSysts.end()) ATH_MSG_WARNING("Could not find systematic " << sv.name());
    }
  }
  
  
  //also check for uncovered recommended systs
  auto recommendedSysts = CP::SystematicRegistry::getInstance().recommendedSystematics();
  for(auto s : recommendedSysts) {
    bool found(false);
    for(auto ss : m_systs) { if(ss.second.find(s)!=ss.second.end()) found=true; }
    for(auto ss : m_sfSysts) { if(ss.find(s)!=ss.end()) found=true; }
    for(auto ss : m_sfSysts2) {if(ss.find(s)!=ss.end()) found=true; }
    if(!found && m_systs.size()>1) ATH_MSG_WARNING("Recommended syst " << s.name() << " not found in list of systs");
  }
  

  return StatusCode::SUCCESS;
}


void ZdZdAnalysisAlg::decorateHelperIsolation(CamVector& lllls) {
   //use isolation helper to get isolation
   static const std::vector<xAOD::Iso::IsolationType> isoTypes = {xAOD::Iso::IsolationType::ptvarcone20,xAOD::Iso::IsolationType::ptvarcone30,xAOD::Iso::IsolationType::topoetcone20};

   
   CamDeque sort_leps; 
   for(auto& llll : lllls) {
         sort_leps.clear();
         for(int i=0;i<2;i++) { 
          for(int j=0;j<2;j++) {
            sort_leps.push_back(llll.child(i)->child(j)); sort_leps.back()->set("lx_idx")=i*2+j; //use lx_idx for 1234-ordered isolation
          }
         }
         sort_leps.sort_Pt();
         
         //decorate the standard flags
         for(auto& tool : m_isoSelectionTools) {
          std::string decorName="is"+tool->name().substr(8,100);
          char result = 0; //stores as bitwise, so 1111 = 15 is all passing, 1000 = 8 is only highest pt passing
          for(int ii=0;ii<4;ii++) result += (sort_leps[ii]->get<bool>(decorName) << (3-ii));
          std::string dName = "l_";dName += decorName;
          llll.set(dName) = result;
         }
         
         for(auto& tool : m_isolHelpers) {
            std::string decorName="l_is"+tool->name().substr(8,100);std::string decorName2="lx_is"+tool->name().substr(8,100);
            char result = 0;char result2 = 0;
            for(int ii=0;ii<4;ii++) {
              bool dec = tool->acceptCorrected(*sort_leps[ii], sort_leps);
              result +=  (int(dec) << (3-ii));
              result2 += (int(dec) << (3-sort_leps[ii]->get<int>("lx_idx")));
              /*if(msgLvl(MSG::VERBOSE) && decorName=="l_isIsolFixedCutLooseHelper" && sort_leps[ii]->get<bool>("isIsolFixedCutLoose")!=dec) {
                ATH_MSG_VERBOSE("Diff found");
                std::vector<float> cors;
                tool->getCloseByCorrection(cors,*sort_leps[ii],isoTypes,sort_leps);
                ATH_MSG_VERBOSE(ii << " ptvarcone20=" << cors[0] << " ptvarcone30=" << cors[1] << " topoetcone20=" << cors[2] << " before=" << sort_leps[ii]->get<bool>("isIsolFixedCutLoose") << " after=" << dec );
              }*/
            }
            llll.set(decorName) = result;
            llll.set(decorName2) = result2; //1234 ordered
          }
    }
      
      
   

   
}


void ZdZdAnalysisAlg::decorateModifiedIsolation(CamVector& lllls) {


  CamDeque sort_leps; 
   for(auto& llll : lllls) {
         sort_leps.clear();
         for(int i=0;i<2;i++) { for(int j=0;j<2;j++) sort_leps.push_back(llll.child(i)->child(j)); }
         sort_leps.sort_Pt();
         for(int i=0;i<4;i++) {
            double subPt(0); double subEt(0);
            for(int j=0;j<4;j++) {
              if(i==j) continue; //don't self-correct leptons
              if(abs(sort_leps[i]->get<int>("pdgId"))==11) {
                //correct ptvarcone20
                if(sort_leps[i]->DeltaR(*sort_leps[j])<std::min(0.2,10000./sort_leps[i]->Pt())) subPt += sort_leps[j]->Pt();
                //correct topoetcone20
                /*try{
                    subEt += (el2->ae<xAOD::Electron>()->caloCluster(0)->rawE()/cosh(el2->ae<xAOD::Electron>()->trackParticle()->eta())); }
                } catch(std::runtime_error&) {
                        //don't do anything because rawE must not be available!
                }*/

              } else {
                //correct ptvarcone30
                if(sort_leps[i]->DeltaR(*sort_leps[j])<std::min(0.3,10000./sort_leps[i]->Pt())) {
                  if(abs(sort_leps[j]->get<int>("pdgId"))==13) {
                    const xAOD::Muon* m = sort_leps[j]->ae<xAOD::Muon>();
                    if(m->muonType()!=xAOD::Muon::MuonStandAlone) subPt += m->trackParticle(xAOD::Muon::InnerDetectorTrackParticle)->pt();
                  } else {
                    subPt += sort_leps[j]->Pt();
                  }
                }
              }
            }
            ///ATH_MSG_VERBOSE("correcting " << i << " with " << subPt << " pt frac = " << sort_leps[i]->get<float&>("ptvarcone30")/sort_leps[i]->Pt() );
            if(abs(sort_leps[i]->get<int>("pdgId"))==11) {
              sort_leps[i]->get<float&>("ptvarcone20") -= subPt;
              sort_leps[i]->get<float&>("topoetcone20") -= subEt;
            } else {
              sort_leps[i]->get<float&>("ptvarcone30") -= subPt;
            }
            ///ATH_MSG_VERBOSE("New frac = " << sort_leps[i]->get<float&>("ptvarcone30")/sort_leps[i]->Pt());
            for(auto& tool : m_isoSelectionTools) {
              std::string decorName="l_is"+tool->name().substr(8,100)+"Modified";
              if(i==0) llll.set(decorName)=char(0);
              llll.set(decorName) = char(llll.get<char>(decorName) + 
                ((abs(sort_leps[i]->get<int>("pdgId"))==11) ? (int(bool(tool->accept( *sort_leps[i]->ae<xAOD::Electron>() ))) << (3-i)) :
                                                              (int(bool(tool->accept( *sort_leps[i]->ae<xAOD::Muon>() ))) << (3-i)) )
                );
              ///ATH_MSG_VERBOSE(decorName << " " << int(llll.get<char>(decorName)));
            }
            if(abs(sort_leps[i]->get<int>("pdgId"))==11) {
              sort_leps[i]->get<float&>("ptvarcone20") += subPt;
              sort_leps[i]->get<float&>("topoetcone20") += subEt;
            } else {
              sort_leps[i]->get<float&>("ptvarcone30") += subPt;
            } 
        }
   }
}

void doSingleObjectTrigMatch( int& passed, int bitShift, const std::string& trig, CamObject* obj, Trig::IMatchingTool& tmt, float thresh) {
  //std::cout << "got here " << trig << " " << bitShift << std::endl;
  if(!(passed&(1<<bitShift))) {
    if(!obj->contains<bool>(trig,true,false)) {
      bool res = (tmt.match(*obj->ae<xAOD::IParticle>(),trig,thresh));
      obj->set(trig) = res;
    }
    passed += (int(obj->get<bool>(trig)) << bitShift);
  }
}


void ZdZdAnalysisAlg::decorateTriggerMatched(CamVector& lllls,CamEvent& evt) {
  int trigDecisions = evt.get<int>("passTriggers");
  //int trigDecisions2016 = evt.get<int>("passTriggers2016");
  bool isMC = evt.get<bool>("isSimulation");


   //trigger match relevent pairs of leptons until we get a match, then flag the 4l candidate as trigger-matched
   for(auto& llll : lllls) {
      std::vector<CamObject*> ls; ls.push_back(llll.child(0)->child(0));ls.push_back(llll.child(0)->child(1));ls.push_back(llll.child(1)->child(0));ls.push_back(llll.child(1)->child(1));
      std::vector<const xAOD::IParticle*> trigPair;
      int passed=0; //int passed2016 = 0;
      for(int i=0;i<4;i++) {
         //try the single lepton trigger 
         
         
         //trigPair.clear();trigPair.push_back(ls[i]->ae<xAOD::IParticle>());
         switch( abs(ls[i]->get<int>("pdgId")) ) {
            case 11: //electron
               for(auto& t : m_myTriggers[11]) {
                if(t.disabled) continue;
                if(!(trigDecisions&(1<<t.idx))) continue;
                doSingleObjectTrigMatch( passed, t.idx, t.getTrigger(!isMC), ls[i], *m_tmt, 0.07);
               }
            /*
               if(trigDecisions2016&(1<<0)) doSingleObjectTrigMatch( passed2016, 0, "HLT_e24_lhtight_nod0_ivarloose", ls[i], *m_tmt, 0.07);
               if(trigDecisions2016&(1<<1)) doSingleObjectTrigMatch( passed2016, 1, "HLT_e60_lhmedium_nod0", ls[i], *m_tmt, 0.07);
               if(trigDecisions&(1<<0)) doSingleObjectTrigMatch( passed, 0, "HLT_e24_lhmedium_L1EM20VH", ls[i], *m_tmt, 0.07);
               if(trigDecisions&(1<<1)) doSingleObjectTrigMatch( passed, 1, "HLT_e60_lhmedium", ls[i], *m_tmt, 0.07);
            */
               break;
            case 13: //muon
              for(auto& t : m_myTriggers[13]) {
                if(t.disabled) continue;
                if(!(trigDecisions&(1<<t.idx))) continue;
                doSingleObjectTrigMatch( passed, t.idx, t.getTrigger(!isMC), ls[i], *m_tmt, 0.1);
               }
            /*
               if(trigDecisions2016&(1<<2)) doSingleObjectTrigMatch( passed2016, 2, "HLT_mu20_iloose_L1MU15", ls[i], *m_tmt, 0.1);
               if(trigDecisions2016&(1<<3)) doSingleObjectTrigMatch( passed2016, 3, "HLT_mu50", ls[i], *m_tmt, 0.1);
               if(trigDecisions&(1<<2)) doSingleObjectTrigMatch( passed, 2, "HLT_mu20_iloose_L1MU15", ls[i], *m_tmt, 0.1);
               if(trigDecisions&(1<<3)) doSingleObjectTrigMatch( passed, 3, "HLT_mu40", ls[i], *m_tmt, 0.1);
             */
               break;
         }
         //if(passed) { llll.set("triggerMatched") = passed; break; }
         //try dilepton
         for(int j=i+1;j<4;j++) {
            trigPair.clear(); trigPair.push_back(ls[i]->ae<xAOD::IParticle>());trigPair.push_back(ls[j]->ae<xAOD::IParticle>());
            //only match with appropriate triggers 
            int pairType = abs(ls[i]->get<int>("pdgId")) + abs(ls[j]->get<int>("pdgId"));
            switch(pairType) {
               case 22: //pair of electrons 
                  for(auto& t : m_myTriggers[22]) {
                    if(t.disabled) continue;
                    if(!(trigDecisions&(1<<t.idx))) continue;
                    if(passed&(1<<t.idx)) continue;
                    passed += ((m_tmt->match(trigPair,t.getTrigger(!isMC),0.07)) << t.idx);
                  }
                  /*
                  if(trigDecisions&(1<<4)) if(!(passed&(1<<4)))passed += ((m_tmt->match(trigPair,"HLT_2e12_lhloose_L12EM10VH")) << 4);
                  if(trigDecisions2016&(1<<4)) if(!(passed2016&(1<<4)))passed2016 += ((m_tmt->match(trigPair,"HLT_2e17_lhvloose_nod0")) << 4);
                  */
                  //do tri-electron trigger too 
                  for(auto& t : m_myTriggers[33]) {
                    if(t.disabled) continue;
                    if(!(trigDecisions&(1<<t.idx))) continue;
                    for(int k=j+1;k<4;k++) {
                      if(passed&(1<<t.idx)) continue;
                      if( abs(ls[k]->get<int>("pdgId"))!=11 ) continue;
                      std::vector<const xAOD::IParticle*> trigTriplet = trigPair;
                      trigTriplet.push_back(ls[k]->ae<xAOD::IParticle>());
                      passed += ((m_tmt->match(trigPair,t.getTrigger(!isMC),0.07)) << t.idx);
                    }
                  }
                  /*
                  
                  if(!(passed2016&(1<<8)) || !(passed&(1<<8))) {
                    for(int k=j+1;k<4;k++) {
                      if( abs(ls[k]->get<int>("pdgId"))!=11 ) continue;
                      std::vector<const xAOD::IParticle*> trigTriplet = trigPair;
                      trigTriplet.push_back(ls[k]->ae<xAOD::IParticle>());
                      if(trigDecisions2016&(1<<8)) if(!(passed2016&(1<<8)))passed2016 += ((m_tmt->match(trigTriplet,"HLT_e17_lhloose_nod0_2e9_lhloose_nod0")) << 8);
                      if(trigDecisions&(1<<8)) if(!(passed&(1<<8)))passed += ((m_tmt->match(trigTriplet,"HLT_e17_lhloose_2e9_lhloose")) << 8);
                    }
                  }
                  */
                  
                  break;
               case 24: //e mu
                  /*
                  if(trigDecisions&(1<<6)) if(!(passed&(1<<6))) passed += ((m_tmt->match(trigPair,"HLT_e17_lhloose_mu14")) << 6);
                  if(trigDecisions2016&(1<<6)) if(!(passed2016&(1<<6))) passed2016 += ((m_tmt->match(trigPair,"HLT_e17_lhloose_nod0_mu14")) << 6);
                  */
                  for(auto& t : m_myTriggers[24]) {
                    if(t.disabled) continue;
                    if(!(trigDecisions&(1<<t.idx))) continue;
                    if(passed&(1<<t.idx)) continue;
                    passed += ((m_tmt->match(trigPair,t.getTrigger(!isMC))) << t.idx);
                  }
                  break;
               case 26: //pair of muons
                  /*
                  if(trigDecisions&(1<<5)) if(!(passed&(1<<5))) passed += ((m_tmt->match(trigPair,"HLT_mu18_mu8noL1")) << 5);
                  if(trigDecisions&(1<<7)) if(!(passed&(1<<7))) passed += ((m_tmt->match(trigPair,"HLT_2mu10")) << 7);
                  if(trigDecisions2016&(1<<5)) if(!(passed2016&(1<<5))) passed2016 += ((m_tmt->match(trigPair,"HLT_mu20_mu8noL1")) << 5);
                  if(trigDecisions2016&(1<<7)) if(!(passed2016&(1<<7))) passed2016 += ((m_tmt->match(trigPair,"HLT_2mu10")) << 7);
                  if(trigDecisions2016&(1<<10)) if(!(passed2016&(1<<10))) passed2016 += ((m_tmt->match(trigPair,"HLT_mu22_mu8noL1")) << 10);
                  
                   //do tri-muon triggers too 
                  if(trigDecisions2016&(1<<9)) if( !(passed2016&(1<<9)) || !(passed&(1<<9)) ) {
                    for(int k=j+1;k<4;k++) {
                      if( abs(ls[k]->get<int>("pdgId"))!=13 ) continue;
                      std::vector<const xAOD::IParticle*> trigTriplet = trigPair;
                      trigTriplet.push_back(ls[k]->ae<xAOD::IParticle>());
                      int res =  ((m_tmt->match(trigTriplet,"HLT_3mu6")) << 9);
                      if(!(passed2016&(1<<9)))passed2016 += res;
                      if(!(passed&(1<<9)))passed += res;
                    }
                  }
                  */
                  
                  for(auto& t : m_myTriggers[26]) {
                    if(t.disabled) continue;
                    if(!(trigDecisions&(1<<t.idx))) continue;
                    if(passed&(1<<t.idx)) continue;
                    passed += ((m_tmt->match(trigPair,t.getTrigger(!isMC))) << t.idx);
                  }
                  //do tri-mu trigger too 
                  for(auto& t : m_myTriggers[39]) {
                    if(t.disabled) continue;
                    if(!(trigDecisions&(1<<t.idx))) continue;
                    for(int k=j+1;k<4;k++) {
                      if(passed&(1<<t.idx)) continue;
                      if( abs(ls[k]->get<int>("pdgId"))!=13 ) continue;
                      std::vector<const xAOD::IParticle*> trigTriplet = trigPair;
                      trigTriplet.push_back(ls[k]->ae<xAOD::IParticle>());
                      passed += ((m_tmt->match(trigPair,t.getTrigger(!isMC))) << t.idx);
                    }
                  }
                  
                  break;
            };
            //llll.set("triggerMatched") = true;
            //break;
         }
         //if(llll.is("triggerMatched")) break;
      }
      llll.set("triggerMatched") = passed;
      //llll.set("triggerMatched2016") = passed2016;
   }
   
}




#include "xAODLuminosity/LumiBlockRangeContainer.h"
#include "EventInfo/EventStreamInfo.h"

#include "TROOT.h"
StatusCode ZdZdAnalysisAlg::beginInputFile() {  
  ATH_MSG_INFO("In begin input file");
  //example of metadata retrieval:
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );

   for(int i=0;i<gROOT->GetListOfFiles()->GetSize();i++) {
         TFile *g = (TFile*)gROOT->GetListOfFiles()->At(i);
         std::cout << "Open file: " << g->GetName() << std::endl;
   }

   ATH_MSG_INFO(inputMetaStore()->dump());

/* doesnt include IncompleteLumiBlocks so I am removing this 
   //if in data, read the luminosity .. and add to the chanInfo for this run ..
   if(inputMetaStore()->contains<xAOD::LumiBlockRangeContainer>("LumiBlocks")) {
      const xAOD::LumiBlockRangeContainer* lbrs = 0;
      CHECK( inputMetaStore()->retrieve(lbrs,"LumiBlocks") );
      //loop over lumiblocks and fill
      for(auto lbr : *lbrs) {
         //keep track of done runs and lb 
         for(uint runNum = lbr->startRunNumber(); runNum <= lbr->stopRunNumber(); runNum++) {
            for(uint lb = lbr->startLumiBlockNumber(); lb <= lbr->stopLumiBlockNumber(); lb++) {
               if(m_doneBlocks[runNum][lb]) continue;
               m_doneBlocks[runNum][lb]=true;
               if(!m_grlTool->passRunLB(runNum,lb)) continue; //don't include a bad lumiblock .. test GRL tool ... optional if the lumicalc file was made with the GRL!
               chanInfo->Fill(runNum, "Lumi", m_prwTool->expert()->GetLumiBlockIntegratedLumi(runNum,lb) );
            }
         }
      }
   }
*/
   
   //get the channel number and update m_isNoSkimChannel
    //get the channelNumber from the EventStreamInfo ...
    const EventStreamInfo* esi = 0;
    CHECK( inputMetaStore()->retrieve(esi) ); //should only be one 
    int chanNum = -1;
    std::cout << "run nums = ";
    for(auto r : esi->getRunNumbers()) std::cout << r << " , ";
    std::cout << std::endl;
    for(auto& type : esi->getEventTypes()) {
        if(type.test(EventType::IS_SIMULATION)) chanNum = type.mc_channel_number();
        else if(esi->getRunNumbers().size()) chanNum = *esi->getRunNumbers().begin(); //just uses the first run number ... there better be one because I don't test here :-(
        break; //only check the first one
    }
    m_isNoSkimChannel=false;
    for(auto c : m_noSkimChannels) {
         if(chanNum==int(c)) {m_isNoSkimChannel=true;break;}
    }

   m_gotBookkeeper=false;
   //look for cutflow bookkeeper ... if found, will use 'sum of events' from that
   if(inputMetaStore()->contains<xAOD::CutBookkeeperContainer>("CutBookkeepers")) {
      const xAOD::CutBookkeeperContainer* bks = 0;
      CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );
      const xAOD::CutBookkeeper* all = 0; int maxCycle=-1;
      for(auto cbk : *bks) {
         if(cbk->inputStream()=="StreamAOD" && cbk->name()=="AllExecutedEvents" && cbk->cycle()>maxCycle ) { maxCycle=cbk->cycle(); all = cbk; }
      }

     

      chanInfo->Fill(chanNum,"Events_All",all->sumOfEventWeights(),all->nAcceptedEvents(),all->sumOfEventWeightsSquared());

      m_gotBookkeeper=true;
   }

  return StatusCode::SUCCESS;
}


#endif //XAOD_ANALYSIS


